package com.example.MyBookShopApp.data;

import javax.persistence.*;

@Entity
@Table(name = "book2user")
public class BookToUser {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne
    private BookToUserType type;

    @ManyToOne
    private Book book;

    @ManyToOne
    private User user;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public BookToUserType getType() {
        return type;
    }

    public void setType(BookToUserType type) {
        this.type = type;
    }

    public Book getBook() {
        return book;
    }

    public void setBook(Book book) {
        this.book = book;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
